**Movies Recommender**

*Installazione*

* Per usare MR bisogna installare alcune dipendenze:
    * pip3 install pandas
    * pip3 install -U scikit-learn
    * pip3 install click
    * pip3 install tmdbsimple

*Uso*

	$ MR [<options> ...]

          Options
            --help, -h              Help
            --movie, -m <int>       Movie ID [Intero tra 0-5999]
            --plot, -p              Mostra la descrizione del film
            --interactive, -i       Mostra info di elaborazione
            --list, -l              Lista dei film
            --recommend, -r <int>   Numero di film consigliati

          Esempi d\'uso
            $ python3 MR.py --help
            $ python3 MR.py -m 5 --recommend 3
            $ python3 MR.py -m 7 -r 3 --plot
            $ python3 MR.py -m 9 -r 3 -p --interactive