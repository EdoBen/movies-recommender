import sys
from os.path import join, dirname
from utility import validateLength, printResults, importData, splitData, isValidMovieID

def listMovies(length):
    # Check whether the input length is valid
    validateLength(length)
    # Import and parse the datasets
    data = importData(verbose=False)
    dataset = splitData(data, verbose=False)
    # Display the movie list
    for i in range(length):
        printResults(
            movieID=i,
            title=dataset['titles'][i],
            plot=dataset['plots'][i],
            showPlots=False)
