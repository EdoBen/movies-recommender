import sys
import time
import pandas as pd
from tfidf import tfidfInit, getSimilarities,getSimilaritiesEuclidean,getSimilaritiesManhattan

moviesCSVPath = "dataset/dataset.csv"
def validateLength(length):
    ## CHECK INDEX ##
    if not (isinstance(length, int) and length >= 1 and length <= 6000):
        print('Invalid value: "' + str(length) + '"')
        print('Input è un intero tra [1, 6000]')
        sys.exit(1)
    return 0

def printResults(movieID, title, plot, showPlots):
    print('=> Title: ' + title + ' - ID: ' + str(movieID))
    if (showPlots):
        print('  Overview:\n' + plot + '\n')

def importData(verbose):
    start = time.time()
    ## IMPORT DATASET ##
    moviesDF = pd.read_csv(moviesCSVPath)
    if (verbose):
        print('Imported Data\t\t{0:.1f}s'.format(time.time() - start))
    return moviesDF


def splitData(moviesDF, verbose):
    start = time.time()
    titles = moviesDF[['Title']].values.flatten().tolist()
    plots = moviesDF[['Plot']].values.flatten().tolist()
    if (verbose):
        print('Split Data\t\t{0:.1f}s'.format(time.time() - start))
    return {'titles': titles, 'plots': plots}

def isValidMovieID(movieID):
    # Check whether the input ID is valid
    if not (isinstance(movieID, int) and movieID >= 0 and movieID <= 5999):
        print('Invalid value for movie ID: "' + str(movieID) + '"')
        print('Movie ID è un intero tra [0, 5999]')
        sys.exit(1)
    return 0

def recommender(movieID=5,
                recommendationsNumber=3,
                showPlots=False,
                interactive=False):
    data = importData(interactive)
    dataset = splitData(data, interactive)
    results = tfidfInit(dataset['plots'], interactive)

    # Similarity functions
    recomendedMovies = getSimilarities(movieID, recommendationsNumber, results,
                                       interactive)
    recomendedMoviesWithEuclidean = getSimilaritiesEuclidean(movieID, recommendationsNumber, results,
                                           interactive)
    recomendedMoviesWithManhattan = getSimilaritiesManhattan(movieID, recommendationsNumber, results,
                                               interactive)
    # Input movie
    print('> Input film')
    printResults(
        movieID=movieID,
        title=dataset['titles'][movieID],
        plot=dataset['plots'][movieID],
        showPlots=showPlots)
    print("\n")

    print('* Top ' + str(recommendationsNumber) + ' Consigliati con tecnica: Cosine Similarity')
    for i in recomendedMovies:
        printResults(
            movieID=i,
            title=dataset['titles'][i],
            plot=dataset['plots'][i],
            showPlots=showPlots)

    print("\n***********************************************************\n")
    print('* Top ' + str(recommendationsNumber) + ' Consigliati con tecnica: distanza Euclidea')
    for i in recomendedMoviesWithEuclidean:
        printResults(
            movieID=i,
            title=dataset['titles'][i],
            plot=dataset['plots'][i],
            showPlots=showPlots)

    print("\n***********************************************************\n")
    print('* Top ' + str(recommendationsNumber) + ' Consigliati con tecnica: Manhattan distance')
    for i in recomendedMoviesWithManhattan:
        printResults(
            movieID=i,
            title=dataset['titles'][i],
            plot=dataset['plots'][i],
            showPlots=showPlots)
    return 0
