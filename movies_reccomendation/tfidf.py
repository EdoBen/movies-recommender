# -*- coding: utf-8 -*-
import time
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics.pairwise import manhattan_distances
from sklearn.feature_extraction.text import TfidfVectorizer
from stop_words import get_stop_words

def tfidfInit(plots, verbose):
    start = time.time()
    stop_words = get_stop_words('italian')
    vectorizer = TfidfVectorizer(
        lowercase=True,
        min_df=3,
        max_df=0.9,
        ngram_range=(1, 2),
        stop_words=stop_words)

    plotsTFIDF = vectorizer.fit_transform(plots)

    if (verbose):
        print('Train step end\t{0:.1f}s'.format(time.time() - start))
    return plotsTFIDF


def getSimilarities(id, recommendations, plotsTFIDF, verbose):
    start = time.time()
    cosineSimilarities = cosine_similarity(plotsTFIDF, plotsTFIDF)
    scores = list(enumerate(cosineSimilarities[id]))
    sortedScores = sorted(scores, key=lambda x: x[1], reverse=True)
    movieRecommendations = sortedScores[1:recommendations + 1]
    movieIndices = [i[0] for i in movieRecommendations]
    print('Found Similarities with Cosine\t{0:.1f}s'.format(time.time() - start))
    return movieIndices

def getSimilaritiesEuclidean(id, recommendations, plotsTFIDF, verbose):
    start = time.time()
    euclidean = euclidean_distances(plotsTFIDF, plotsTFIDF)
    scores = list(enumerate(euclidean[id]))
    sortedScores = sorted(scores, key=lambda x: x[1], reverse=True)
    movieRecommendations = sortedScores[1:recommendations + 1]
    movieIndices = [i[0] for i in movieRecommendations]
    print('Found Similarities with Euclidean\t{0:.1f}s'.format(time.time() - start))
    return movieIndices

def getSimilaritiesManhattan(id, recommendations, plotsTFIDF, verbose):
    start = time.time()
    manhattan = manhattan_distances(plotsTFIDF, plotsTFIDF)
    scores = list(enumerate(manhattan[id]))
    sortedScores = sorted(scores, key=lambda x: x[1], reverse=True)
    movieRecommendations = sortedScores[1:recommendations + 1]
    movieIndices = [i[0] for i in movieRecommendations]
    print('Found Similarities with Manhattan\t{0:.1f}s'.format(time.time() - start))
    return movieIndices
