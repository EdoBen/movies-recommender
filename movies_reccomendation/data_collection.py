import json
import csv
import random
from time import time
import tmdbsimple as tmdb

## API KEY TMDB ##
tmdb.API_KEY = '69406a55f9d71ed8bcf976248b8a183d'

## PATH DATASET ##
path_csv = "/dataset/data.csv"
path_partial_dataset = "/dataset/dataset.csv"

PARTIAL_DATASET = 6000
popular_movies = tmdb.Movies().popular(language="en-US")
total_page = popular_movies['total_pages']
total_movies = popular_movies['total_results']
start_time = time()
movie = 1

with open(path_csv, "w") as outfile:
    f = csv.writer(outfile)
    f.writerow(["id", "Title", "Plot"])
    for number_page in range(1,int(total_page)):
        list_movie = tmdb.Movies().popular(language="en-US", page=number_page)
        for i in list_movie["results"]:
            if i['overview'] == "":
                continue
            movie = movie + 1
            print ("Movie {0}/{1}".format(movie,total_movies))
            f.writerow([movie, i['title'], i['overview']])
end_time = time()
elapsed_time = end_time - start_time
print("Total time to create complete dataset: {0:0.2f} sec".format(elapsed_time))

print("***** Extract 6000 random movie from data.csv *****")
start_time = time()
random_movies = random.sample(range(0, movie), 6000)

## CREATE PARTIAL DATASET ##
csv_file = csv.reader(open(path_csv, "r"), delimiter=",")
with open(path_partial_dataset, "w") as outfile:
    f = csv.writer(outfile)
    f.writerow(["id", "Title", "Plot"])
    for row in csv_file:
        for i in range(0,len(random_movies)):
            if row[0] == str(random_movies[i]):
                f.writerow([row[0], row[1], row[2]])

end_time = time()
elapsed_time = end_time - start_time
print("Total time to create partial dataset: {0:0.2f} sec".format(elapsed_time))
