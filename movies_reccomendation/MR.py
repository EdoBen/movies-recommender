import click
import sys
from movie import listMovies
from utility import recommender

@click.command()
@click.option('-m', '--movie', default=2874, help='movie ID')
@click.option('-p', '--plot', is_flag=True, default=False, help='Mostra la descrizione del film')
@click.option(
    '-i',
    '--interactive',
    is_flag=True,
    default=False,
    help='Mostra info di elaborazione')
@click.option('-h', '--help', is_flag=True, default=False, help='Help')
@click.option('-l', '--list', is_flag=True, default=False, help='Lista dei film')
@click.option('-r', '--recommend', default=3, help='Numero di film consigliati')

def main(movie, recommend, plot, interactive, help, list):
    if(help):
        print('''
          Progetto SMM - Movie recommender

          Uso
            $ MR [<options> ...]

          Options
            --help, -h              Help
            --movie, -m <int>       Movie ID [Can be any integer 0-4999]
            --plot, -p              Mostra la descrizione del film
            --interactive, -i       Mostra info di elaborazione
            --list, -l              Lista dei film
            --recommend, -r <int>   Numero di film consigliati [Intero tra 1-30]

          Esempi d'uso
            $ MR --help
            $ MR -m 5 --recommend 3
            $ MR -m 7 -r 3 --plot
            $ MR -m 9 -r 3 -p --interactive
        ''')
        sys.exit(0)
    else:
        if(list):
            listLength = click.prompt('=> Numero di film da visualizzare', type=int)
            listMovies(length=listLength)
        else:
            if(recommend):
                recommender(movieID=movie,
                            recommendationsNumber=recommend,
                            showPlots=plot,
                            interactive=interactive)



if __name__ == '__main__':
    main()
